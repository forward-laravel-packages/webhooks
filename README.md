# Forward API Package for Laravel

## Installation
You must have added your ssh key to your SSH agent in order to install this private package.

This Package can be installed via Composer, you must add the package name in your composer.json file manually in the require object like this:

``` json
"require": {
    "php" : "~5.6|~7.0",
    "forward/webhook": "0.1.*"
},
```
In the same composer.json file you must add the repositories object to provide to composer a valid place to search for this package

``` json
"repositories":[
  {
    "type": "git",
    "url": "git@gitservfwd.forward.com.mx:blacked/package-forward-webhooks.git"
  }
]
```

Then just simply run the update command in your terminal

``` bash
$ composer clearcache
$ composer update
```

Next add the WebhookServiceProvider to your config/app.php file.

``` php
  Forward\Webhook\WebhookServiceProvider::class,
```

Run the Migrations command to create the necessary webhooks tables

``` bash
$ php artisan migrate
```

For *Laravel 5.1* you must publish the migrations with this command

``` bash
$ php artisan vendor:publish --provider="Forward\Webhook\WebhookServiceProvider" --tag="migrations"
```

That's all.

## Configuration

You need to specify the Webhook Request Header where you are going to specify the Webhook Token.

In your .env file you can specify this value, that depends on your repository server, by default the request header is the Gitlab Token Header.

```
WEBHOOK_REQUEST_HEADER=X-Gitlab-Token
```

## Usage

All the interactions with the webhooks are via artisan, the available commands are:

- webhook:new For creating a new webhook.
- webhook:list For listing all the available webhooks.
- webhook:update For modify a webhook.
- webhook:delete Delete a webhook.

## Security

If you discover any security related issues, please email edwin@forward.com.mx instead of using the issue tracker.

## Credits

- [Edwin Moedano][edwin@forward.com.mx]
