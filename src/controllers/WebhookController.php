<?php

namespace Forward\Webhook\Controllers;

use Request;
use Response;
use Forward\Webhook\Models\Hook;
use App\Http\Controllers\Controller;

class WebhookController extends Controller
{
  /**
   * Execute a webhook
   *
   * @param  string   $token
   * @return Response
   */
  public function execute()
  {
      try {
        if($token = Request::header(config('forward-webhook.webhooks_request_header'))){
          // If no webhook was found, return a 404
          if (!$hook = Hook::findByTokenAndMethod($token, Request::method())->first()) {
            return Response::json('webhook_not_found', 404);
          }
          // Otherwise queue the script for execution, and return a 200
          $hook->queueScript();
          return Response::json('webhook_executed_successfully', 200);
        }else{
          return Response::json('webhook_token_missing', 400);
        }
      } catch (\Exception $e) {
          return Response::json([
            'message' => 'webhook_failed',
            'errors' => [$e->getMessage()]
          ], 500);
      }
  }
}
