<?php

namespace Forward\Webhook\Models;

use Queue;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\Model;
use App\Models\Model;

class Hook extends Model
{
    protected $table = 'webhooks';

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'script',
        'http_method',
        'is_enabled',
        'request_header'
    ];
    /**
     * @var array Attribute casting
     */
    protected $casts = [
        'is_enabled' => 'boolean',
    ];

    protected $appends = ['enabled'];

    /**
     * @var boolean Incrementing ID
     */
    public $incrementing = false;

    public function logs(){
      return $this->hasMany('Forward\Webhook\Models\Log');
    }

    public function getEnabledAttribute(){
        return $this->is_enabled ? 'true' : 'false';
    }

    public function toArray(){
        $attributes = $this->attributesToArray();
        $attributes = array_merge($attributes, $this->relationsToArray());
        unset($attributes['is_enabled']);
        return $attributes;
    }

    /**
     * Execute the script and log the output
     *
     * @return string
     */
    public function queueScript()
    {
        return $this->executeScript();
        //$id = $this->id;
        //Queue::push(function() use ($id) { Hook::findAndExecuteScript($id); });
    }

    /**
     * Execute the shell script and log the output
     *
     * @return string
     */
    public function executeScript()
    {
        // Make sure the script is enabled
        if (!$this->is_enabled) {
            throw new ScriptDisabledException();
        }
        // Run the script and log the output
        $output = exec($this->script.' 2>&1;');
        //Create UUID
        $uuid = Uuid::generate(4);
        //Create Log
        Log::create(['id' => (string) $uuid->string,'hook_id' => $this->id, 'output' => $output]);
        // Update our executed_at timestamp
        $this->executed_at = Carbon::now();
        $this->save();
        return $output;
    }

    /**
     * Returns the script with normalized line endings
     *
     * @return void
     */
    public function getScriptAttribute($script)
    {
        return preg_replace('/\r\n?/', PHP_EOL, $script);
    }

    /**
     * Find a hook by token and HTTP method
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  string                               $token
     * @param  string                               $httpMethod
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFindByTokenAndMethod($query, $token, $httpMethod) {
      return $query->whereIsEnabled(true)
            ->whereHttpMethod($httpMethod)
            ->whereToken($token);
    }

    /**
    * Find a hook and execute it's script
    *
    * @param  \October\Rain\Database\Builder   $query
    * @param  integer                          $id
    * @return \October\Rain\Database\Builder
    */
   public function scopeFindAndExecuteScript($query, $id)
   {
       return $query->find($id)->executeScript();
   }

   /**
     * Enables or disables webhooks
     *
     * @param  \October\Rain\Database\Builder   $query
     * @return integer
     */
    public function scopeSetIsEnabled($query, $isEnabled)
    {
        return $query->update([
            'is_enabled' => $isEnabled,
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
    * Helper for snake_case http method
    *
    * @return string
    */
   public function getHttpMethodAttribute()
   {
       return array_key_exists('http_method', $this->attributes)
           ? $this->attributes['http_method']
           : 'post';
   }
}
