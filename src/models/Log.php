<?php

namespace Forward\Webhook\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'webhook_logs';
    /**
     * @var array Fillable fields
     */
    protected $fillable = ['id', 'hook_id', 'output'];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var boolean Incrementing ID
     */
    public $incrementing = false;

    public function hook(){
       return $this->belongsTo('Forward\Webhook\Hook');
    }
}
