<?php

/**
 * WebHooks
 */
Route::get('webhooks','Forward\Webhook\Controllers\WebhookController@execute');
Route::post('webhooks','Forward\Webhook\Controllers\WebhookController@execute');
