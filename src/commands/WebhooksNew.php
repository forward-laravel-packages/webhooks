<?php

namespace Forward\Webhook\Commands;

use Webpatser\Uuid\Uuid;
use Illuminate\Console\Command;
use Forward\Webhook\Models\Hook;

class WebhooksNew extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'webhook:new
            {--autodeploy} {--name=} {--script=} {--http_method=} {--activate}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create and activate and New Webhook';

  /**
   * A Default Deploy Webhook data structure
   * @var array
   */
  protected $deploy_data = [
    'name' => 'AuthAPI Deploy',
    'http_method'  => 'POST',
    'script' => 'cd /var/www/git/forward-api.git/ && git fetch && GIT_WORK_TREE=/var/www/html/ git checkout -f master & cd /var/www/html/ & composer dump-autoload'
  ];

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    try{
        if($this->option('autodeploy')){
          $hook = new Hook($this->deploy_data);
          $hook->is_enabled = $this->option('activate') ? true : $this->confirm('Will this Hook be activated?');
        }else{
          $hook = new Hook([
            'name' => $this->option('name') ? $this->option('name') : $this->ask('Give this Hook a name'),
            'http_method' => $this->option('http_method') ? $this->option('http_method') : $this->choice('What Http would you use?', ['GET', 'POST'], 1),
            'script' => $this->option('script') ? $this->option('script') :$this->ask('Write down your evil script'),
            'is_enabled' => $this->option('activate') ? true : $this->confirm('Will this Hook be activated?')
          ]);
        }
        //Create a "Random" but Unique Token
        do {
            $hook->token = str_random(40);
        } while (Hook::where('token', $hook->token)->exists());
        //Set The UUID
        $uuid = Uuid::generate(4);
        $hook->id = (string) $uuid->string;
        //Save The Hook
        if($hook->save()){
          $this->info('Webhook created successfully with token: '.$hook->token);
        }
    }catch(\Exception $e){
      $this->error('Something went wrong with the Webhook '.$e->getMessage());
    }
  }
}
