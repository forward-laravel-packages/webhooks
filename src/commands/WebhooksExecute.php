<?php

namespace Forward\Webhook\Commands;

use Illuminate\Console\Command;
use Forward\Webhook\Models\Hook;

class WebhooksExecute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webhook:execute {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute a webhooks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      try{
          $id = $this->argument('id') ? $this->argument('id') : $this->ask('Hook Id');
          if($hook = Hook::find($id)){
            $this->line($hook->queueScript());
            $this->comment('Hook executed successfully');
          }
      }catch(\Exception $e){
          $this->error('Something went wrong '.$e->getMessage());
      }
    }
}
