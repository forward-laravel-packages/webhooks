<?php

namespace Forward\Webhook\Commands;

use Illuminate\Console\Command;
use Forward\Webhook\Models\Hook;

class WebhooksList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webhook:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all the webhooks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if(Hook::count() > 0){
        $hooks = Hook::select(['id', 'name', 'http_method','token','is_enabled'])->get();
        $this->table(
          ['Id', 'Name', 'Http Method', 'token','Enabled'],
          $hooks->toArray()
        );
      }
      $this->info('No Webhooks available');
    }
}
