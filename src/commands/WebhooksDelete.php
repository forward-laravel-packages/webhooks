<?php

namespace Forward\Webhook\Commands;

use Illuminate\Console\Command;
use Forward\Webhook\Models\Hook;

class WebhooksDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webhook:delete {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a Webhook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      try{
          $id = $this->option('id') ? $this->option('id') : $this->ask('Hook Id');
          if($hook = Hook::find($id)){
            if($this->confirm('Do you really want to delete the hook ['.$hook->name.']')){
              $hook->delete();
            }
          }
      }catch(\Exception $e){
          $this->error('Something went wrong '.$e->getMessage());
      }
    }
}
