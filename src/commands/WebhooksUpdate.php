<?php

namespace Forward\Webhook\Commands;

use Illuminate\Console\Command;
use Forward\Webhook\Models\Hook;

class WebhooksUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webhook:update {id?} {--name=} {--http_method=} {--activate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edit some webhook by Id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $id = $this->argument('id') ? $this->argument('id') : $this->ask('Hook Id');
            if($hook = Hook::find($id)){
                $hook->name = $this->option('name') ? $this->option('name') : $this->ask("Hook's name", $hook->name);
                $hook->http_method = $this->option('http_method') ? $this->option('http_method') : $this->choice('What Http would you use?', ['GET', 'POST'], 1);
                $hook->script = $this->ask('Write down your evil script', $hook->script);
                $hook->is_enabled = $this->option('activate') ? true : $this->confirm('Will this Hook be activated?', $hook->is_enabled);
                if($hook->save()){
                    $this->info('Hook updated successfully with token '.$hook->token);
                }
            }
        }catch(\Exception $e){
            $this->error('Something went wrong '.$e->getMessage());
        }
    }
}
