<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Forward Webhooks
     |--------------------------------------------------------------------------
     |
     */
    'webhooks_request_header' => env('WEBHOOK_REQUEST_HEADER', 'X-Gitlab-Token')
];
