<?php

namespace Forward\Webhook;

use Illuminate\Support\ServiceProvider;
use Forward\Webhook\Commands\WebhooksNew;
use Forward\Webhook\Commands\WebhooksList;
use Forward\Webhook\Commands\WebhooksUpdate;
use Forward\Webhook\Commands\WebhooksDelete;
use Forward\Webhook\Commands\WebhooksExecute;

class WebhookServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $version = explode('.', app()::VERSION);

        if($version[1] == '1'){
          //Fix for Laravel 5.1
          require __DIR__.'/routes.php';
          //Publish the migrations
          $this->publishes([
               __DIR__.'/../database/migrations/' => database_path('migrations')
          ], 'migrations');
        }else{
            //Load the Routes file
            $this->loadRoutesFrom(__DIR__.'/routes.php');
            //Load the migrations directory
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }
        //Load the API Module Commands
        if ($this->app->runningInConsole()) {
          $this->commands([
              WebhooksNew::class,
              WebhooksList::class,
              WebhooksUpdate::class,
              WebhooksDelete::class,
              WebhooksExecute::class
          ]);
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/forward-webhook.php', 'forward-webhook'
        );
    }
}
